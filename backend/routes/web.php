<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PropertyController;

Route::get('/', function () {
    return redirect()->route('properties');
});

Route::get('/home', [App\Http\Controllers\PropertyController::class, 'mvcProperties'])->name('properties');

Route::get('/home', [PropertyController::class, 'create'])->name('properties');
Route::get('/property', [PropertyController::class, 'index'])->name('properties.index');
Route::post('/property', [PropertyController::class, 'store'])->name('properties.store');
Route::get('/property/{property}', [PropertyController::class, 'show'])->name('properties.show');
