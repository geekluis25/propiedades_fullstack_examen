/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************!*\
  !*** ./resources/js/mapa.js ***!
  \******************************/
var map;

function initMap() {
  // The location of Uluru
  var uluru = {
    lat: 19.4561691,
    lng: -99.1916377
  }; // The map, centered at Uluru

  var map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    center: uluru
  }); // The marker, positioned at Uluru

  var marker = new google.maps.Marker({
    position: uluru,
    map: map,
    label: ""
  });
  var infoWindow = new google.maps.InfoWindow({
    content: "",
    disableAutoPan: true
  });
  marker.addListener("click", function () {
    infoWindow.setContent("<h1>hola mundo 🦁</h1> <p>como estas</p>  <p>como estas</p>  <p>como estas</p>");
    infoWindow.open(map, marker);
  });
}
/******/ })()
;