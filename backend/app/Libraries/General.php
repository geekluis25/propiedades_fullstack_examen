<?php

namespace App\Libraries;
use Illuminate\Support\Facades\DB;

class General {
    /**
     * Metodo para recuperar los valores de un campo enum
     * @param $table la tabla en la que esta el enum
     * @param $column la columna que es el enum
     * @return array
     */
    public static function getEnumValues($table, $column) {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $values = array();
        foreach(explode(',', $matches[1]) as $value){
            $values[] = trim($value, "'");
        }
        return $values;
    }
}