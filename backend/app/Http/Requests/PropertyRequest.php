<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'property_type' => 'required',
            'operation' => 'required',
            'state' => 'required',
            'city' => 'required',
            'neighborhood' => 'required',
            'cp' => 'required',
            'street' => 'required',
            'latitude' => ["required", "regex:/^(\+|-)?(?:90(?:(?:\.0{1,7})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,7})?))$/"],
            'longitude' => ["required", "regex:/^(\+|-)?(?:180(?:(?:\.0{1,7})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,7})?))$/"],
            'num_bathrooms' => 'required',
            'bedrooms' => 'required',
            'm2_construction' => 'required',
            'parking' => 'required',
            'age' => 'required',
            'departments' => 'required',
            'floor' => 'required',
            'amenities' => 'required',
            'path' => 'required',
        ];
    }

    public function messages(){
        return [
            // Validacion de requeridos
            'name.required' => 'El :attribute es requerido',
            'description.required' => 'La :attribute es requerida',
            'price.required' => 'El :attribute es requerido',
            'property_type.required' => 'El :attribute es requerido',
            'operation.required' => 'El :attribute es requerido',
            'state.required' => 'El :attribute es requerido',
            'city.required' => 'El :attribute es requerido',
            'neighborhood.required' => 'El :attribute es requerido',
            'cp.required' => 'El :attribute es requerido',
            'street.required' => 'El :attribute es requerido',
            'latitude.required' => 'El :attribute es requerido',
            'longitude.required' => 'El :attribute es requerido',
            'num_bathrooms.required' => 'El :attribute es requerido',
            'bedrooms.required' => 'El :attribute es requerido',
            'm2_construction.required' => 'El :attribute es requerido',
            'parking.required' => 'El :attribute es requerido',
            'age.required' => 'El :attribute es requerido',
            'departments.required' => 'El :attribute es requerido',
            'floor.required' => 'El :attribute es requerido',
            'amenities.required' => 'La :attribute es requerida',
            'path.required' => 'Se requiere al menos una imagen',

            'latitude.regex' => 'El formato de :attribute es incorrecto',
            'longitude.regex' => 'El formato de :attribute es incorrecto'
        ];
    }

    public function attributes(){
        return [
            'name' => 'Nombre',
            'description' => 'Descripción',
            'price' => 'Precio',
            'property_type' => 'Tipo de propiedad',
            'operation' => 'Operacion',
            'state' => 'Estado',
            'city' => 'Ciudad',
            'neighborhood' => 'Vecindario',
            'cp' => 'Codigo postal',
            'street' => 'Calle',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'num_bathrooms' => 'Numero de baños',
            'bedrooms' => 'Dormitorios',
            'm2_construction' => 'm2 de construcción',
            'parking' => 'estacionamiento',
            'age' => 'años',
            'departments' => 'departamentos',
            'floor' => 'piso',
            'amenities' => 'Comodidad'
        ];
    }
}
