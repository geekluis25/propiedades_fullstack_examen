<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\Amenity;
use App\Models\PropertyAmenity;
use App\Models\PropertyImage;

use Illuminate\Support\Facades\DB;
use App\Libraries\General;
use App\Http\Requests\PropertyRequest;


use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{
    public function index(){
        // $propeties = Property::with(
        //     'propertyAmenity.amenity',
        //     'propertyImage'
        // )->get();

        $propeties = Property::all();
        // return $propeties;

        return view('index',compact('propeties'));
    }
    public function create()
    {
        // consultamos los enum de la tabla
        $property_type = General::getEnumValues('properties','property_type') ;
        $operation = General::getEnumValues('properties','operation') ;
        // consultamos amenities
        $amenities = Amenity::all();
        // retornamos los valores
        return view('properties',compact('property_type', 'operation', 'amenities'));
    }
    public function show(Property $property){
        //return $property;
        return view('show',compact('property'));
    }

    public function store(PropertyRequest $request){
        //dd( $request->all() );
        // ultimo registro de al tabla property
        $ultimo = Property::latest('id')->select('id')->first();
        // creamos el public_key de 4 digitos y lo agregamos al request
        $request->merge([
            'public_key' => str_pad($ultimo->id + 1, 4, "0", STR_PAD_LEFT ),
            'user' => 1
        ]);
         // iniciamos el proceso de transacciones para evitar errores de insert
         DB::beginTransaction();
        // registramos la propiedad
        try {
            $registro = Property::create( $request->except('amenities') );
            foreach ($request->amenities as $valor) {
                PropertyAmenity::create([
                    'property_id' => $registro->id,
                    'amenity_id' => $valor
                ]);
            }
            // registramos las imagenes
            foreach ($request->path as $clave => $valor) {
                PropertyImage::create([
                    'path' => $valor,
                    'order' => $clave + 1,
                    'property' => $registro->id,
                ]);
            }
            // hacemos el commit
            DB::commit();
        } catch ( Throwable $e) {
            DB::rollback();
            return response()->json([
                'status' => 'false',
                'mensaje' => 'El registro fallo, no se inserto la información'
            ]);
        }
        return $registro;
    }
}
