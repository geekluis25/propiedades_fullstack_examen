@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif

<form class="row mt-5 mb-5" id="form">
    @csrf
    <div class="col-6 col-sm-6 mb-3">
        <label for="name" class="form-label">Nombre:</label>
        <input type="text" class="form-control" id="name" name="name" value="">
        <div id="name-validate" class="invalid-feedback"></div>
    </div>
    <div class="col-6 col-sm-6 mb-3">
        <label for="description" class="form-label">Descripción:</label>
        <input type="text" class="form-control" id="description" name="description" value="">
        <div id="description-validate" class="invalid-feedback"></div>
    </div>
    <div class="col-6 col-sm-6 mb-3">
        <label for="price" class="form-label">Precio</label>
        <input type="text" class="form-control" id="price" name="price" value="">
        <div id="price-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="property_type" class="form-label">Tipo de propiedad</label>
        <select class="form-select" name="property_type" id="property_type">
            <option value="" selected>-- Seleccionar tipo --</option>
            @foreach ($property_type as $item)
                <option value="{{ $item }}">{{ $item }}</option>    
            @endforeach
        </select>
        <div id="property_type-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="operation" class="form-label">Operacion</label>
        <select class="form-select" name="operation" id="operation">
            <option value="" selected>-- Seleccionar operacion --</option>
            @foreach ($operation as $item)
                <option value="{{ $item }}">{{ $item }}</option>    
            @endforeach
        </select>
        <div id="operation-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="state" class="form-label">Estado:</label>
        <input type="text" class="form-control" id="state" name="state">
        <div id="state-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="city" class="form-label">Ciudad:</label>
        <input type="text" class="form-control" id="city" name="city">
        <div id="city-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="neighborhood" class="form-label">Vecindario:</label>
        <input type="text" class="form-control" id="neighborhood" name="neighborhood">
        <div id="neighborhood-validate" class="invalid-feedback"></div>
    </div>


    <div class="col-6 col-sm-6 mb-3">
        <label for="cp" class="form-label">Codigo postal:</label>
        <input type="text" class="form-control" id="cp" name="cp">
        <div id="cp-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="street" class="form-label">Calle:</label>
        <input type="text" class="form-control" id="street" name="street">
        <div id="street-validate" class="invalid-feedback"></div>
    </div>


    <div class="col-6 col-sm-6 mb-3">
        <label for="latitude" class="form-label">Latitude:</label>
        <input type="text" class="form-control" id="latitude" name="latitude">
        <div id="latitude-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="longitude" class="form-label">Longitude:</label>
        <input type="text" class="form-control" id="longitude" name="longitude">
        <div id="longitude-validate" class="invalid-feedback"></div>
    </div>


    <div class="col-6 col-sm-6 mb-3">
        <label for="num_bathrooms" class="form-label">Numero de baños:</label>
        <input type="text" class="form-control" id="num_bathrooms" name="num_bathrooms">
        <div id="num_bathrooms-validate" class="invalid-feedback"></div>
    </div>


    <div class="col-6 col-sm-6 mb-3">
        <label for="bedrooms" class="form-label">Dormitorios:</label>
        <input type="text" class="form-control" id="bedrooms" name="bedrooms">
        <div id="bedrooms-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="m2_construction" class="form-label">m2 de construcción:</label>
        <input type="text" class="form-control" id="m2_construction" name="m2_construction">
        <div id="m2_construction-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="parking" class="form-label">estacionamiento:</label>
        <input type="text" class="form-control" id="parking" name="parking">
        <div id="parking-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="age" class="form-label">años:</label>
        <input type="text" class="form-control" id="age" name="age">
        <div id="age-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="departments" class="form-label">departamentos:</label>
        <input type="text" class="form-control" id="departments" name="departments">
        <div id="departments-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="floor" class="form-label">piso:</label>
        <input type="text" class="form-control" id="floor" name="floor">
        <div id="floor-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-6 col-sm-6 mb-3">
        <label for="amenities" class="form-label">Comodidades</label>
        <select class="form-select" name="amenities" id="amenities" multiple aria-label="multiple select example">
            @foreach ($amenities as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>    
            @endforeach
          </select>
        <div id="amenities-validate" class="invalid-feedback"></div>
    </div>

    <div class="col-12 col-sm-12 mb-3">
        <h3 for="amenities" class="form-label">Imagenes para agregar</h3>
        <p>Selecciona la imagen que quieran para esta propiedad.</p>
        <label class="checkeable">
            <input type="checkbox" id="path" name="path" value="" class="path"/>
            <img src="{{ asset('images/default.png') }}" class="imageSelect img-thumbnail"/>
        </label>
        <label class="checkeable">
            <input type="checkbox" name="path" value="" class="path"/>
            <img src="{{ asset('images/default.png') }}" class="imageSelect img-thumbnail"/>
        </label>
        <label class="checkeable">
            <input type="checkbox" name="path" value="" class="path"/>
            <img src="{{ asset('images/default.png') }}" class="imageSelect img-thumbnail"/>
        </label>
        <label class="checkeable">
            <input type="checkbox" name="path" value="" class="path"/>
            <img src="{{ asset('images/default.png') }}" class="imageSelect img-thumbnail"/>
        </label>
        <label class="checkeable">
            <input type="checkbox" name="path" value="" class="path"/>
            <img src="{{ asset('images/default.png') }}" class="imageSelect img-thumbnail"/>
        </label>
        <div id="path-validate"></div>
        {{-- <div id="amenities-validate" class="invalid-feedback"></div> --}}
    </div>

    
    <div class="d-grid gap-2">
        <button type="submit" class="btn btn-primary btn-lg">Submit</button>
    </div>
    
</form>