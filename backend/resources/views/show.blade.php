@extends('layouts.app')
 
@section('title', 'Page Title')
 

@section('content')
    <h1>Propiedad</h1>
    {{-- {{ $property }} --}}
    <div class="wrapperImage" style="background: url('{{ $property->propertyImage[0]->path }}');background-position: center;background-size: cover;">
        <div class="info">
            <h3>{{ $property->name }}</h3>
            <h4>{{ $property->neighborhood }}</h4>
            <h4>{{ $property->property_type }}</h4>
            <h4>$ {{ $property->price }}</h4>
        </div>
    </div>


    <div class="owl-carousel owl-theme imagesCarousel">
        @foreach ($property->propertyImage as $item)
            <div class="item">
                <img src="{{$item->path}}" alt="">
            </div>
        @endforeach
    </div>

    <div class="detalles">
        <h1>Detalles del inmueble</h1>
        {{ $property->description }}
        <h2>Caracteristicas</h2>
        <ul>
            @foreach ($property->propertyAmenity as $item)
                <li>{{$item->amenity->name}}</li>
            @endforeach
        </ul>
        
    </div>


    <div class="wrapperMapa">
        <div id="map"></div>
    </div>
    
   
@endsection


@push('scripts')
<script>
    $(document).ready(function(){
        $(".owl-carousel").owlCarousel({
            center: true,
            items:2,
            loop:true,
            margin:10,
            nav:true,
        });
    });
    
</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR2HB3K5ETqT6kAzxUnJKxtsNl21bakX4&callback=initMap&v=weekly" async></script>
    <script>
        let map;
        function initMap() {
            // The location of Uluru
            const uluru = { lat: {{ $property->latitude }}, lng: {{ $property->longitude }} };
            // The map, centered at Uluru
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 16,
                center: uluru,
            });
            // The marker, positioned at Uluru
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                label: ""
            });
            const infoWindow = new google.maps.InfoWindow({
                content: "",
                disableAutoPan: true,
            });
            marker.addListener("click", () => {
                infoWindow.setContent("<h1>{{ $property->name }}</h1> <p>Precio: $ {{ $property->price }}</p>  <p>{{ $property->property_type }}</p> ");
                infoWindow.open(map, marker);
            });
        }
    </script>
 
@endpush