@extends('layouts.app')
 
@section('title', 'Page Title')
 

@section('content')
    <h1>Propiedades</h1>
    {{-- {{ $propeties }} --}}
    <div class="row mt-5 mb-5">
        @foreach ($propeties as $item)
        <div class="col-12 col-sm-6 col-lg-4 mb-3">
            <div class="card">
                <img src="{{ $item->propertyImage[0]->path }}" class="rounded mx-auto d-block img-thumbnail image" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ $item->name }}</h5>
                    <p class="card-text">{{ $item->description }}</p>
                    <p class="card-text">${{ $item->price }}</p>
                    <a href="{{ route('properties.show',$item->id) }}" class="btn btn-primary">Ver más</a>
                </div>
            </div>
        </div>

        @endforeach
        
    </div>
@endsection


@push('scripts')

@endpush