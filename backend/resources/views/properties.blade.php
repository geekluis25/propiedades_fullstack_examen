@extends('layouts.app')
 
@section('title', 'Page Title')
 

@section('content')
    <h1>Registro de propiedades</h1>
    @include('partials.formulario')
@endsection


@push('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/inputmaskFunctions.js') }}"></script>
    <script src="{{ asset('js/registerProperty.js') }}"></script>
@endpush