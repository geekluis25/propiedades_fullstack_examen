//https://api.pexels.com/v1/search?query=house&orientation=square&size=small&per_page=2
//563492ad6f917000010000012731069895f146a98606d16a054239e2
// Importamos Swal para usar las notificaciones
import Swal from 'sweetalert2';
// importamos la validacion del formulario general
import { validarForm } from './validarForm.js';
// importamos la notificacion por sweetalert2
import { notificacion } from './notificacion.js';

import { createClient } from 'pexels';

// Se importan metodos que se utilizan
import { 
    limpiarErrores, 
    notificarErrores, 
    notificarErroresTelefono,
    notificarErroresEditarUsuario
} from './errores.js';

/**** Variables  *****/
const formProperty = document.querySelector('#form');




/****** Funciones ******/

// Funcion para agregar propiedad
const addProperty = async ( e ) => {
    e.preventDefault();
    // creamos el formulario
    const form = crearFormulario();
    // validamos que el formulario no tenga registros vacios
    // limpiamos los errores del formulario
    limpiarErrores( form );
    const validar = validarForm( form );
    if( validar == false ){
        notificacion( 'Error...!', 'Uno o mas campos estan vacios', 'error' );
    }else{
        // en caso de que no existan campos vacios realizamos la peticion
        const respuesta = await crearRegistro( form );
        // si existe un id en la respuesta
        if( respuesta.id ){
            // este metodo solo pasa datos al swal para notificar.
            notificacion( 'Bien...!', 'Registro correcto', 'success' );
            addImages();
        }else{
            // este metodo solo pasa datos al swall para notificar.
            notificacion( 'Error...!', 'Uno o mas campos tienen error', 'error' );
            // este metodo activa los errores en el formulario y sele pasa el
            // arreglo de errores que retorna el servicio con codigo 422
            notificarErrores( respuesta.errors );
        }
    }
}

// 
const addImages = async () => {

    // seleccionamos las imagenes del formulario
    const imageSelect = document.getElementsByClassName("imageSelect");
    const path = document.getElementsByClassName("path");
   
    // implementamos la api de pexels
    const client = createClient('563492ad6f917000010000012731069895f146a98606d16a054239e2');
    const page = Math.floor(Math.random() * 5) + 1;;
    const query = `house&orientation=square&size=small&page=${page}`;
    const response = await client.photos.search({ query, per_page: 5 });
    const data = await response.photos;
    // Recorremos las imagenes para crear un nuevo objeto de imagenes que solo
    // contendra la imagen original
    const images = data.map(function (item, index, array) {
        return item.src.original
    });

    // cambiamos el src de las imagenes por las que se recibieron de pixel
    for( let i=0; i < imageSelect.length; i++ ){
        path[i].value = images[i];
        imageSelect[i].src = images[i];
    }
}

// Metodo para enviar informacion al servicio y generar el contacto
const crearRegistro = async ( form ) => {
    try{
        // se realiza peticion con axios
        const response = await axios.post("/property", form);
        const data = await response.data;
        // limpiiamos el formulario y cerramos la modal
        document.getElementById("form").reset();
        return data;
    }catch (err) {
        // en caso de retornar un 422 de validacion del formulario
        // console.log(err.response.data)
        return err.response.data;
    }
}



// Metodo para crear la informacion del formulario
const crearFormulario = () => {
    const form = {
        //_token" => "t5LcsaaZjQwtwwBkzGEoH0QT8JLd2K8GMCC6bGDf"
        "name": document.querySelector('#name').value.trim(),
        "description": document.querySelector('#description').value.trim(),
        "price": document.querySelector('#price').value.trim(),
        "property_type": document.querySelector('#property_type').value.trim(),
        "operation": document.querySelector('#operation').value.trim(),
        "state": document.querySelector('#state').value.trim(),
        "city": document.querySelector('#city').value.trim(),
        "neighborhood": document.querySelector('#neighborhood').value.trim(),
        "cp": document.querySelector('#cp').value.trim(),
        "street": document.querySelector('#street').value.trim(),
        "latitude": document.querySelector('#latitude').value.trim(),
        "longitude": document.querySelector('#longitude').value.trim(),
        "num_bathrooms": document.querySelector('#num_bathrooms').value.trim(),
        "bedrooms": document.querySelector('#bedrooms').value.trim(),
        "m2_construction": document.querySelector('#m2_construction').value.trim(),
        "parking": document.querySelector('#parking').value.trim(),
        "age": document.querySelector('#age').value.trim(),
        "departments": document.querySelector('#departments').value.trim(),
        "floor": document.querySelector('#floor').value.trim(),
        
    }
    
    // recuperamos los amenities que pueden ser usados
    const select = document.getElementById('amenities');
    if(select.selectedOptions.length == 0){
        form.amenities = "";
    }else{
        const selected = [...select.selectedOptions].map(option => option.value);
        // agregamos los valores seleccionaddos
        form.amenities = selected;
    }

    // creamos el objeto de imagenes
    let values = [];
    const checkboxes = document.querySelectorAll('.path')
    checkboxes.forEach((checkbox) => {
        if( checkbox.checked ){
            values.push(checkbox.value);
        }
        
    });
    form.path = values;
    //alert(values);
    

    return form;
}


/****** AddEvents ******/

const addEventsListener = () =>{
    formProperty.addEventListener("submit", addProperty);
}
// Iniciamos los eventos
addEventsListener();
addImages();