import Inputmask from "inputmask";

var name = document.getElementById("name");
var description = document.getElementById("description");
var price = document.getElementById("price");
var state = document.getElementById("state");
var city = document.getElementById("city");
var neighborhood = document.getElementById("neighborhood");
var cp = document.getElementById("cp");
var street = document.getElementById("street");
var latitude = document.getElementById("latitude");
var longitude = document.getElementById("longitude");
var num_bathrooms = document.getElementById("num_bathrooms");
var bedrooms = document.getElementById("bedrooms");
var m2_construction = document.getElementById("m2_construction");
var parking = document.getElementById("parking");
var age = document.getElementById("age");
var departments = document.getElementById("departments");
var floor = document.getElementById("floor");


Inputmask({ regex: "[a-zA-Z- ]*" }).mask(name);
Inputmask({ regex: "[a-zA-Z-1-9 ]*" }).mask(description);
Inputmask({ "mask": "9", "repeat": 6 }).mask(price);
Inputmask({ regex: "[a-zA-Z- ]*" }).mask(state);
Inputmask({ regex: "[a-zA-Z- ]*" }).mask(city);
Inputmask({ regex: "[a-zA-Z-1-9 ]*" }).mask(neighborhood);
Inputmask({"mask": "99999"}).mask(cp);
Inputmask({ regex: "[a-zA-Z-1-9 ]*" }).mask(street);
Inputmask({ regex: "^-?([1-9]?[1-9]|[1-9]0)\\.{1}\\d{1,7}" }).mask(latitude);
Inputmask({ regex: "^-?([1-9]?[1-9]|[1-9]0)\\.{1}\\d{1,7}" }).mask(longitude);
Inputmask({"mask": "9"}).mask(num_bathrooms);
Inputmask({"mask": "9"}).mask(bedrooms);
Inputmask({"mask": "999"}).mask(m2_construction);
Inputmask({"mask": "9"}).mask(parking);
Inputmask({"mask": "9", "repeat": 3}).mask(age);
Inputmask({"mask": "9"}).mask(departments);
Inputmask({"mask": "9"}).mask(floor);
