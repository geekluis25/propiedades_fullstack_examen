let map;

function initMap() {
   // The location of Uluru
  const uluru = { lat: 19.4561691, lng: -99.1916377 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    center: uluru,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
    label: ""
  });
  const infoWindow = new google.maps.InfoWindow({
    content: "",
    disableAutoPan: true,
  });
  marker.addListener("click", () => {
    infoWindow.setContent("<h1>hola mundo 🦁</h1> <p>como estas</p>  <p>como estas</p>  <p>como estas</p>");
    infoWindow.open(map, marker);
  });
}